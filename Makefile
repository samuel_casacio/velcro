include Common.mk

#List of subsystem folders
SUBSYSTEMS := src h .

#Generator for list of resultant object files, source, includes
define GEN_info = 
	$(1)_OBJ := $(patsubst $(1)/%.cpp,$(1)/$(OBJ_PATH)/%.o,$(wildcard $(1)/*.cpp))
	$(1)_SRC := $(wildcard $(1)/*.cpp)
	$(1)_INC := $(wildcard $(1)/*.h)
	ALL_OBJ_PATHS += $(1)/$(OBJ_PATH)
	ALL_OBJ += $$($(1)_OBJ)
	ALL_SRC += $$($(1)_SRC)
	ALL_INC += $$($(1)_INC)
endef

#Generator for make rules of each subsystem
define GEN_rules =
$(1)/$(OBJ_PATH)/%.o : $(1)/%.cpp $(1)/$(OBJ_PATH)/.depend $(ALL_INC)
	$(CXX) $$< $(CXX_FLAGS) -o $$@
endef

#Run info generator
$(foreach subsys,$(SUBSYSTEMS),$(eval $(call GEN_info,$(subsys))))

#Dependency of final program on object files; link
$(BIN_PATH)velcro : $(ALL_OBJ) $(BIN_PATH)
	$(LD) $(LD_FLAGS) -o "$(BIN_PATH)velcro" $(ALL_OBJ)

#Run make rule generator
$(foreach subsys,$(SUBSYSTEMS),$(eval $(call GEN_rules,$(subsys))))

$(BIN_PATH):
	mkdir -p $(BIN_PATH)

#Generate folder for compiled objects, as well as handling basic depends
%/.depend : 
	-mkdir -p $(@D)
	touch $@

#Generation of documentation
doc : $(ALL_SRC) $(ALL_INC) doc/Doxyfile
	doxygen doc/Doxyfile

#Clean
clean:
	-$(RM) $(ALL_OBJ)
	-$(RM) $(ALL_OBJ_PATHS)
	-$(RM) $(BIN_PATH)velcro
	-$(RM) $(DOC_PATH)html
	-$(RM) $(DOC_PATH)latex

#Calculate line count
lines:
	@echo "velcro contains" `cat */*.cpp */*.h | wc -l` "lines of code"
	@echo " in `ls */*.cpp | wc -l` source files and `ls */*.h | wc -l` header files"

.PRECIOUS: %/.depend
.PHONY: clean doc lines
