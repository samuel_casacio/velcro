Author: Samuel Casacio

Overview

velcro is a half-edge mesh based data structure.
While the structure can theoretically handle any type of mesh,
currently only triangle meshes are supported.

There is a very basic test in velcro.cpp.  The makefile will compile to <project>/bin/velcro.

Generated documentation can be found in <project>/doc/html/index.html

To-Do

cppunit tests for each class in source
Reimplement query to use SWIFT, SWIFT++, or GJK with depth testing to handle ClosestPointQuery-s on faces with more than 3 vertices
Linearization of BVH memory
Stream based backing store for Mesh when memory usage exceeds system memory
SSE for Vector operations

Compiling

This code has been tested with g++ 4.8.3 on Fedora 20.
Dependencies as follows:

libc
libstdc++
libgcc_s
libm
Make
doxygen 1.8.6+ (for documentation)
graphviz (for documentation)

compile/link main program:
$> make

generate documentation:
$> make doc
cleanup workspace:
$make clean

ClosestPointQuery

The ClosestPointQuery class (CPQ) uses a backing BVH tree to speed up queries against meshes.
First CPQ queries the BVH tree for all the triangles within the lowest levels closest to the specified distance.
Then CPQ iterates over those triangles tracking the closest point, which it then returns at the end.
