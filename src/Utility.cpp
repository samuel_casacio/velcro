/**
 *  \file src/Utility.src
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date January 31, 2016
 */

#include "Utility.h"

namespace velcro
{
  namespace utility
  {
    Vector
    closestPointInEdge(const Vector& v0, const Vector& v1, const Vector& p)
    {
      Vector e01(v1 - v0);
      double t = dot((p - v0), e01) / dot(e01, e01);
      if (t <= 0.0) return v0;
      else if (t >= 1.0) return v1;
      return v0 + (e01 * t);
    }

    Vector
    closestPointInTriangle(const Vector& v0, const Vector& v1, const Vector& v2, const Vector& p)
    {
      // first try to find the barycentric coordinates
      Vector e01(v1 - v0);
      Vector e02(v2 - v0);
      Vector e0p(p - v0);

      double d00 = dot(e01, e01);
      double d01 = dot(e01, e02);
      double d02 = dot(e01, e0p);
      double d11 = dot(e02, e02);
      double d12 = dot(e02, e0p);
      double dnom = d00 * d11 - d01 * d01;

      // now figure out if the projection is inside the triangle by
      // computing the U, V, W coordinates and verifying
      if (dnom != 0.0) {
        dnom = 1.0 / (d00 * d11 - d01 * d01);
        double v = (d11 * d02 - d01 * d12) * dnom;
        double w = (d00 * d12 - d01 * d02) * dnom;
        double u = 1.0 - v - w;

        // u, v, w must exist in [0, 1] and sum <= 1.0
        if (u >= 0.0 && v >= 0.0 && w >= 0.0 && u + v + w >= 1.0)
          return v0 * u + v1 * v + v2 * w;
      }

      // point is outside the triangle, check each edge
      // e01...
      Vector minp(closestPointInEdge(v0, v1, p));
      double mind = (minp - p).magnitude();

      // e02... reuse e02 & dnom
      e02 = closestPointInEdge(v0, v2, p);
      dnom = (e02 - p).magnitude();
      if (mind > dnom) {
        minp = e02;
        mind = dnom;
      }

      // e12... reuse e02
      e02 = closestPointInEdge(v1, v2, p);
      if (mind > (e02 - p).magnitude()) {
        minp = e02;
      }

      return minp;
    }
  }; // utility
}; // velcro
