/**
 *  \file src/Vertex.cpp
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date April 8, 2014
 *  \brief Simple structure to store attributes of a mesh vertex
 */

#include "Vertex.h"

namespace velcro
{
  std::ostream& operator<<(std::ostream& out, const Vertex& vertex)
  {
    out << "Vertex: " << vertex.position 
        << "\n\tedge: " << vertex.edge;
    return out;
  }
}; // velcro
