/**
 *  \file h/ClosestPointQuery.h
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date February 2, 2016
 */

#include <limits>
#include <vector>
#include <iostream>

#include "BoundingVolumeHierarchy.h"
#include "ClosestPointQuery.h"
#include "Mesh.h"
#include "Index.h"
#include "Vector.h"

namespace velcro
{
  ClosestPointQuery::ClosestPointQuery(const Mesh& m):
    bvh(m)
  {
    // empty
  }

  Vector
  ClosestPointQuery::operator()(const Vector& p, double dist) const
  {
    double tmp = std::numeric_limits<double>::infinity();
    Vector ret(tmp, tmp, tmp);
    Vector sel;

    // first get the subset of faces to try
    std::vector<FaceIndex> faces(bvh.search(p, dist));

    std::cout << "found: " << faces.size() << " faces\n";

    // for each face found, check the distance and track to closest
    for (auto ndx : faces) {
      sel = bvh.getMesh().closestPointInFace(p, ndx);
      if ((sel - p).magnitude() < tmp) {
        ret = sel;
        tmp = (sel - p).magnitude();
      }
    }
    
    return ret;
  }
}; // velcro
