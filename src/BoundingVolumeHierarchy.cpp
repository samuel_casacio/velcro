/**
 *  \file src/BoundingVolumeHierarchy.cpp
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date February 2, 2016
 */

#include <algorithm>
#include <memory>
#include <vector>

#include "BoundingVolumeHierarchy.h"
#include "Index.h"

namespace velcro
{
  BVH::BVH(const Mesh& m):
    mesh(m), primitives()
  {
    std::vector<BVHPrimitiveInfo> buildData;
    buildData.reserve(mesh.numberFaces());
    primitives.reserve(mesh.numberFaces());

    for (auto fit = mesh.faceBegin(); fit != mesh.faceEnd(); ++fit) {
      AABB bbox = mesh.getFaceBoundingVolume(fit.idx());
      primitives.push_back(fit.idx());
      buildData.push_back(BVHPrimitiveInfo(fit.idx().raw(), bbox));
    }

    uint32_t totalNodes = 0;
    std::vector<FaceIndex> orderedPrims;
    orderedPrims.reserve(mesh.numberFaces());

    root = recursiveBuild(buildData, 0, mesh.numberFaces(),
                          &totalNodes, orderedPrims);

    primitives.swap(orderedPrims);
  }


  std::shared_ptr<BVH::BVHBuildNode>
  BVH::recursiveBuild(std::vector<BVHPrimitiveInfo> &buildData,
                      uint32_t start, uint32_t end, uint32_t *totalNodes,
                      std::vector<FaceIndex>& orderedPrims)
  {
    (*totalNodes)++;

    std::shared_ptr<BVHBuildNode> node(new BVHBuildNode);

    // compute the bounds of all primitives in node
    AABB bbox;
    for (uint32_t i = start; i < end; ++i) {
      bbox.merge(buildData[i].box);
    }

    uint32_t nPrimitives = end - start;
    if (nPrimitives <= 10) {

      // create leaf
      uint32_t firstPrimOffset = orderedPrims.size();
      for (uint32_t i = start; i < end; ++i) {
        uint32_t primNum = buildData[i].primitiveNumber;
        orderedPrims.push_back(primitives[primNum]);
      }

      node->initLeaf(firstPrimOffset, nPrimitives, bbox);

    } else {
      AABB centroidBounds;
      for (uint32_t i = start; i < end; ++i) {
        centroidBounds.merge(buildData[i].box.centroid());
      }

      int dim = centroidBounds.maximumExtent();
      if (centroidBounds.max()[dim] == centroidBounds.min()[dim]) {

        // create leaf
        uint32_t firstPrimOffset = orderedPrims.size();
        for (uint32_t i = start; i < end; ++i) {
          uint32_t primNum = buildData[i].primitiveNumber;
          orderedPrims.push_back(primitives[primNum]);
        }

        node->initLeaf(firstPrimOffset, nPrimitives, bbox);
        return node;
      }

      uint32_t mid = (start + end) / 2;
      std::nth_element(&buildData[start], &buildData[mid],
                       &buildData[end - 1] + 1, ComparePoints(dim));

      node->initInterior(dim,
                         recursiveBuild(buildData, start, mid, totalNodes, orderedPrims),
                         recursiveBuild(buildData, mid, end, totalNodes, orderedPrims));
    }

    return node;
  }


  void
  BVH::recursiveSearch(const Vector& v, double dist,
                       std::vector<FaceIndex>& faces, const std::shared_ptr<BVHBuildNode>& node) const
  {
    // base case, node is too far away
    if ((dist != 0.0 && node->box.distance(v) > dist) || (dist == 0.0 && !node->box.inside(v))) {
      return;
    }

    // next case, node is a leaf node
    // just append the faces to the list
    if (node->nPrimitives > 0) {
      for (uint32_t i = node->firstPrimOffset; i < node->firstPrimOffset + node->nPrimitives; ++i) {
        faces.push_back(primitives[i]);
      }
      return;
    }

    // recurse on 2 children
    recursiveSearch(v, dist, faces, node->children[0]);
    recursiveSearch(v, dist, faces, node->children[1]);
  }

  std::vector<FaceIndex>
  BVH::search(const Vector& v, double dist) const
  {
    std::vector<FaceIndex> faces;
    recursiveSearch(v, dist, faces, root);
    return faces;
  }

}; // velcro
