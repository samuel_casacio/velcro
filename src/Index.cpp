/**
 *  \file src/Index.cpp
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date April 8, 2014
 *  \brief Contains declarations of the Index type
 */

#include "Index.h"

namespace velcro
{
  std::ostream& operator<<(std::ostream& out, const Index& index)
  {
    out << "Index [group, value, valid]: [" << index.group() << ", " 
      << index.offset() << ", "
      << index.valid() << "]";
    return out;
  }
}; // velcro

