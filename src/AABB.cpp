/**
 *  \file src/AABB.cpp
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date January 31, 2016
*/

#include <algorithm>
#include <cfloat>
#include <limits>

#include "AABB.h"
#include "Utility.h"

namespace velcro
{
  AABB::AABB(const Vector& min, const Vector& max):
    bounds{min, max}
  {
    updateCenter();
  }
   
  AABB::AABB(const AABB& box):
    bounds{box.bounds[0], box.bounds[1]}
  {
    updateCenter();
  }

  AABB::AABB()
  {
    double tmp = std::numeric_limits<double>::max();
    bounds[0] = Vector(tmp, tmp, tmp);
    tmp = std::numeric_limits<double>::lowest();
    bounds[1] = Vector(tmp, tmp, tmp);
  }

  double
  AABB::distance(const Vector& point) const
  {
    // first check if the point is inside the BB
    if (inside(point)) {
      return 0.0;
    }

    // not inside bounds, query all possible planes and return the
    // distance.  can potentially be speed up by using GJK...

    // first find the closest X plane
    Vector px(utility::closestPointOnPlane(bounds[0], Vector(1.0, 0.0, 0.0), point));
    double xdist = (px - point).magnitude();
    Vector tmp(utility::closestPointOnPlane(bounds[1], Vector(1.0, 0.0, 0.0), point));

    // take the closest of the 2 x planes
    if (xdist > (tmp - point).magnitude()) {
      px = tmp;
      xdist = (tmp - point).magnitude();
    }
    
    // if we are in the bounds of the box, stop
    if (inside(px)) {
      return xdist;
    }

    // now for the 2 y planes
    Vector py(utility::closestPointOnPlane(bounds[0], Vector(0.0, 1.0, 0.0), point));
    double ydist = (py - point).magnitude();
    tmp = utility::closestPointOnPlane(bounds[1], Vector(0.0, 1.0, 0.0), point);

    // take the closest of the 2 y planes
    if (ydist > (tmp - point).magnitude()) {
      py = tmp;
      ydist = (tmp - point).magnitude();
    }
    
    // if we are in the bounds of the box, stop
    if (inside(py)) {
      return ydist;
    }

    // finally the 2 z planes
    Vector pz(utility::closestPointOnPlane(bounds[0], Vector(0.0, 0.0, 1.0), point));
    double zdist = (pz - point).magnitude();
    tmp = utility::closestPointOnPlane(bounds[1], Vector(0.0, 0.0, 1.0), point);

    // take the closest of the 2 z planes
    if (zdist > (tmp - point).magnitude()) {
      pz = tmp;
      zdist = (tmp - point).magnitude();
    }
    
    // if we are in the bounds of the box, stop
    if (inside(pz)) {
      return zdist;
    }

    // none of the projected points are contained within the BB...
    // closest point is on an edge, so take the closest distance and cap
    // coordinates to the volume of the box
    // store the closest projection in px...
    if (ydist < xdist) {
      px = py;
      xdist = ydist;
    }

    if (zdist < xdist) {
      px = pz;
    }

    px.X() = std::min(bounds[1].X(), std::max(bounds[0].X(), px.X()));
    px.Y() = std::min(bounds[1].Y(), std::max(bounds[0].Y(), px.Y()));
    px.Z() = std::min(bounds[1].Z(), std::max(bounds[0].Z(), px.Z()));

    return (px - point).magnitude();
  }
 
  void
  AABB::merge(const Vector& point)
  {
    bounds[0] = Vector(std::min(point.X(), bounds[0].X()),
                       std::min(point.Y(), bounds[0].Y()),
                       std::min(point.Z(), bounds[0].Z()));
    bounds[1] = Vector(std::max(point.X(), bounds[1].X()),
                       std::max(point.Y(), bounds[1].Y()),
                       std::max(point.Z(), bounds[1].Z()));
    updateCenter();
  }
  
  void
  AABB::merge(const AABB& box)
  {
    merge(box.bounds[0]);
    merge(box.bounds[1]);
    updateCenter();
  }

  void
  AABB::reset(const Vector& min, const Vector& max)
  {
    bounds[0] = min;
    bounds[1] = max;
    updateCenter();
  }

  void
  AABB::reset(const AABB& box)
  {
    bounds[0] = box.bounds[0];
    bounds[1] = box.bounds[1];
    updateCenter();
  }

  int
  AABB::maximumExtent() const
  {
    Vector diag = bounds[1] - bounds[0];
    if (diag.X() > diag.Y() && diag.X() > diag.Z()) {
      return 0;
    }
    if (diag.Y() > diag.Z()) {
      return 1;
    }
    return 2;
  }

  std::ostream&
  operator<<(std::ostream& out, const AABB& rhs)
  {
    out << "min: " << rhs.min() << std::endl;
    out << "max: " << rhs.max() << std::endl;
    out << "center: " << rhs.centroid();
    return out;
  }
}; // velcro
