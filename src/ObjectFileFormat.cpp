/**
 *  \file src/ObjectFileFormat.cpp
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date May 14th, 2014
 *  \brief Simple off loader and saver
 */

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <map>
#include <cstring>
#include <string>

#include "ObjectFileFormat.h"

namespace velcro
{
  bool LoadOff(const char* filename, Mesh* mesh)
  {
    std::ifstream file(filename, std::ifstream::in);
    std::string line;
    char *ltok;
    uint64_t nv, ne, nf;
    std::map<uint64_t, VertexIndex> verts;
    uint64_t counter = 0;

    if(!file.is_open()) return false;
    
    // off files start with OFF printed at the top of the file
    // followed by a bunch of comments starting with # or whitespace
    // the data for the mesh starts with a line giving the number
    // of vertices, faces, and edges (though the edges is ignored normally)
    // vertices are listed first, followed by the faces
    // face data starts with the number of vertices in the face
    // followed by each vertex index starting with 0
    getline(file, line);
    if(line != "OFF"){
      file.close();
      return false;
    }
    
    while(getline(file, line)){
      ltok = strtok(&line[0], "\r\n \t");
      if(ltok == NULL) continue;
      else if(!strcmp(&ltok[0], "#")) continue;
      else{
        sscanf(ltok, "%ld", &nv);
        ltok = strtok(NULL, " \t");
        sscanf(ltok, "%ld", &nf);
        ltok = strtok(NULL, " \t");
        sscanf(ltok, "%ld", &ne);
        break;
      }
    }
    
    for(uint64_t n = 0; n < nv; ++n){
      getline(file, line);
      Vertex v;
      sscanf(&line[0], "%lf %lf %lf", &v.position.X(), &v.position.Y(), &v.position.Z());
      verts.emplace(counter++, mesh->addVertex(v));
    }
    for(uint64_t n = 0; n < nf; ++n){
      getline(file, line);
      std::vector<VertexIndex> face;
      ltok = strtok(&line[0], " \t");
      ltok = strtok(NULL, " \t");

      while(ltok != NULL){
        sscanf(ltok, "%ld", &ne);
        face.push_back(verts[ne]);
        ltok = strtok(NULL, " \t");
      }

      mesh->createFace(face);
    }

    file.close();
    return true;
  }

  bool SaveOff(const char* filename, const Mesh& mesh)
  {
    std::map<VertexIndex, int> verts;
    FILE* file = fopen(filename, "w");
    int counter = 0;

    if(file == NULL) return false;
    
    fprintf(file, "OFF\n");
    fprintf(file, "# OFF Created By Velcro\n\n");
    fprintf(file, "%ld %ld %ld\n", mesh.numberVerts(), mesh.numberFaces(), mesh.numberEdges());
  
    for(auto vitr(mesh.vertexCBegin()); vitr != mesh.vertexCEnd(); ++vitr){
      verts.emplace(vitr.idx(), counter++);
      fprintf(file, "%lf %lf %lf\n", 
        vitr->position.X(), vitr->position.Y(), vitr->position.Z());
    }
    
    for(auto fitr(mesh.faceBegin()); fitr != mesh.faceEnd(); ++fitr){
      int size = 0;
      for(auto fvitr(mesh.faceVertexCBegin(fitr.idx()));
        fvitr != mesh.faceVertexCEnd(fitr.idx()); ++fvitr
      ){
        ++size;
      }
      
      fprintf(file, "%d ", size);
      for(auto fvitr(mesh.faceVertexCBegin(fitr.idx()));
        fvitr != mesh.faceVertexCEnd(fitr.idx()); ++fvitr
      ){
        fprintf(file, "%d ", verts.find(fvitr.idx())->second);
      }
      fprintf(file, "\n");
    }
  
    fclose(file);
    return true;
  }
}; // velcro
