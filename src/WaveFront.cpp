/**
 *  \file src/WaveFront.cpp
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date April 27, 2014
 *  \brief Simple obj loader and saver
 */

#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <map>
#include <cstring>
#include <string>

#include "WaveFront.h"

namespace velcro
{

  bool LoadObj(const char* filename, Mesh* mesh)
  {
    std::ifstream file(filename, std::ifstream::in);
    std::string line;
    char *ltok, *vtok;
    if(!file.is_open()) return false;

    std::map<int, VertexIndex> verts;
    int counter = 0;

    while( getline(file, line) ){
    
      ltok = strtok(&line[0], "\r\n \t");
      if (ltok == NULL) continue;
      
      if (!strcmp(ltok, "v")) {
        ltok = strtok(NULL, "\r\n");
        Vertex v;
        sscanf(ltok, "%lf %lf %lf", &v.position.X(), &v.position.Y(), &v.position.Z());
        verts.emplace(++counter, mesh->addVertex(v));
      } else if (!strcmp(ltok, "f")) {
        std::vector<VertexIndex> face;
        ltok = strtok(NULL, "\r\n");
        vtok = strtok(ltok, " \t");
        while (vtok != NULL) {
          face.push_back(verts[atoi(vtok)]);
          vtok = strtok(NULL, " \t");
        }
        mesh->createFace(face);
      }
    }
    file.close();
    return true;
  }

  bool SaveObj(const char* filename, const Mesh& mesh)
  {
    std::map<VertexIndex, int> verts;
    FILE* file = fopen(filename, "w");
    int counter = 1;

    if(file == NULL) return false;
    
    fprintf(file, "#Wavefront OBJ Created By Velcro\n");
  
    for(auto vitr(mesh.vertexCBegin()); vitr != mesh.vertexCEnd(); ++vitr){
      verts.emplace(vitr.idx(), counter++);
      fprintf(file, "v %lf %lf %lf\n", vitr->position.X(), vitr->position.Y(), vitr->position.Z());
    }
    
    fprintf(file, "\n");
    
    for(auto fitr(mesh.faceBegin()); fitr != mesh.faceEnd(); ++fitr){
      fprintf(file, "f ");
      for(auto fvitr(mesh.faceVertexCBegin(fitr.idx()));
        fvitr != mesh.faceVertexCEnd(fitr.idx()); ++fvitr
      ){
        fprintf(file, "%d ", verts.find(fvitr.idx())->second);
      }
      fprintf(file, "\n");
    }
  
    fclose(file);
    return true;
  }
}; // velcro
