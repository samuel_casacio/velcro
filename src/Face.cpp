/**
 *  \file src/Face.cpp
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date April 8, 2014
 *  \brief Simple structure to store attributes of a mesh face
 */

#include "Face.h"

namespace velcro
{
  std::ostream& operator<<(std::ostream& out, const Face& face)
  {
    out << "Face:\t"
        << "\eDedge: " << face.edge;
    return out;
  }
}; // velcro
