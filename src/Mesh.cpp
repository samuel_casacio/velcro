/**
 *  \file src/Mesh.cpp
 *  \author Samuel Casacio
 *  \version 1.2
 *  \date January 29, 2016
 *  \brief Simple half-edge mesh structure
 *  
 *  Note: The backing memory store has been abstracted to a Mem::InCore object.
 *        This is to facilitate future experiments in using cached or paged memory
 *        as the backing store.
 *
 *  TODO: Remove triangle only mesh restriction by implementing GJK.
 */

#include <algorithm>
#include <limits>
#include <stdexcept>

#include "Mesh.h"
#include "Utility.h"

namespace velcro
{
  Mesh::Mesh():
    m_Storage(), m_NVerts(0), m_NEdges(0), m_NFaces(0)
  {
    // empty
  }

  VertexIndex
  Mesh::addVertex(const Vertex& vertex)
  {
    ++m_NVerts;
    return m_Storage.storeVertex(vertex);
  }

  FaceIndex
  Mesh::createFace(const std::vector<VertexIndex>& verts)
  {
    // mesh can handle any number of verts in a face, but
    // for this project it can only handle triangles.
    if (verts.size() != 3) {
      throw std::invalid_argument("number of verts in face != 3");
    }

    ++m_NFaces;
    FaceIndex face = m_Storage.storeFace(Face());

    // the edges in the face need to be winded in the same
    // order as the parameter list.
    // to do this, we track the very first edge in the face
    // and the last edge we used.
    // if we have a last edge, we point it's nextEdgeInFace
    // for the new face to the current EdgeIndex (ei).
    // once we break out of the loop, we connect the very last
    // edge in the face to the first edge in the face.
    EdgeIndex first;
    EdgeIndex last;

    for(size_t ndx = 0; ndx < verts.size(); ++ndx){
    
      // get the current VertexIndex pair representing the
      // current edge.  if the edge doesnt exist (the 
      // EdgeIndex is not valid), create a new edge and
      // set it up.  Otherwise retrieve the edge from
      // memory and update it's opposite face.
      VertexIndex vi0(verts[ndx]);
      VertexIndex vi1(verts[(ndx+1) % verts.size()]);
      EdgeIndex ei(lookupEdge(vi0, vi1));
      
      if(!ei.valid()){
        Edge edge;

        edge.face[0] = face;
        edge.vertex[0] = vi0;
        edge.vertex[1] = vi1;
 
        Vertex& v0(m_Storage.getVertex(vi0));
        Vertex& v1(m_Storage.getVertex(vi1));

        edge.nextEdgeAtVertex[0] = v0.edge;
        edge.nextEdgeAtVertex[1] = v1.edge;

        ++m_NEdges;
        ei = m_Storage.storeEdge(edge);
        
        v0.edge = ei;
        v1.edge = ei;
      }
      
      else{
        Edge& edge(m_Storage.getEdge(ei));
        edge.face[1] = face;
      }
      
      if(last.valid()){
        Edge& lastEdge(m_Storage.getEdge(last));
        lastEdge.nextEdgeInFace[lastEdge.face[0] == face ? 0 : 1] = ei;
      }
      else{
        first = ei;
      }
      
      last = ei;
    }
    
    m_Storage.getFace(face).edge = first;
    
    Edge& edge(m_Storage.getEdge(last));
    edge.nextEdgeInFace[edge.face[0] == face ? 0 : 1] = first;
    
    return face;
  }

  EdgeIndex
  Mesh::lookupEdge(VertexIndex v0, VertexIndex v1) const
  {
    EdgeIndex current = m_Storage.getVertex(v0).edge;

    while(current != EdgeIndex::Invalid()){
      const Edge& edge(m_Storage.getEdge(current));
      if(edge.contains(v1)) break;
      current = edge.next(v0);
    }
    
    return current;
  }

  Vector
  Mesh::normal(FaceIndex fi) const
  {
    const Face &face = m_Storage.getFace(fi);
    const Edge &e0(m_Storage.getEdge(face.edge));
    const Edge &e1(m_Storage.getEdge(e0.next(fi)));

    // need to figure out vertex ordering
    VertexIndex vi0, vi1, vi2;
    if(e1.contains(e0.vertex[0])){
      vi0 = e0.vertex[1];
      vi1 = e0.vertex[0];
      vi2 = e1.opposite(vi1);
    }
    else{
      vi0 = e0.vertex[0];
      vi1 = e0.vertex[1];
      vi2 = e1.opposite(vi1);
    }
    
    // get vertices
    const Vertex &v0(m_Storage.getVertex(vi0));
    const Vertex &v1(m_Storage.getVertex(vi1));
    const Vertex &v2(m_Storage.getVertex(vi2));
    Vector normal;

    normal = cross(v0.position - v1.position, v2.position - v1.position);
    normal.normalize();

    return normal;
  }

  AABB
  Mesh::getFaceBoundingVolume(FaceIndex face) const
  {
    double temp = std::numeric_limits<double>::max();
    Vector min(temp, temp, temp);
    temp = std::numeric_limits<double>::lowest();
    Vector max(temp, temp, temp);
    Vector pos;

    for(auto fvit(faceVertexCBegin(face)); fvit != faceVertexCEnd(face); ++fvit) {
      pos = fvit->position;
      min.X() = std::min(min.X(), pos.X());
      min.Y() = std::min(min.Y(), pos.Y());
      min.Z() = std::min(min.Z(), pos.Z());

      max.X() = std::max(max.X(), pos.X());
      max.Y() = std::max(max.Y(), pos.Y());
      max.Z() = std::max(max.Z(), pos.Z());
    }

    return AABB(min, max);
  }

  Vector
  Mesh::closestPointInFace(const Vector& point, FaceIndex face) const
  {
    auto fvit = faceVertexCBegin(face);

    Vector v0(fvit->position);

    ++fvit;
    Vector v1(fvit->position);

    ++fvit;
    Vector v2(fvit->position);

    return utility::closestPointInTriangle(v0, v1, v2, point);
  }
};  // velcro
