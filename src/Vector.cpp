/**
 *  \file src/Vector.cpp
 *  \author Samuel Casacio
 *  \version 1.4
 *  \date October 9th, 2013
 *  \brief 3 dimensional vector class
 */

#include <cmath>
#include "Vector.h"

namespace velcro
{
  // c++11 constructors
  Vector::Vector(double nx, double ny, double nz):
   m_Data{nx, ny, nz} { }

  Vector::Vector(const Vector &rhs):
   m_Data{rhs.m_Data[0], rhs.m_Data[1], rhs.m_Data[2]} { }

  double Vector::magnitude() const
  {
    return( sqrt(m_Data[0] * m_Data[0] + m_Data[1] * m_Data[1] + m_Data[2] * m_Data[2]) );
  }

  Vector Vector::unitvector() const
  {
    return( *this / magnitude() );
  }

  void Vector::normalize()
  {
    double mag = 1.f / magnitude(); 
    m_Data[0] = m_Data[0] * mag; 
    m_Data[1] = m_Data[1] * mag;
    m_Data[2] = m_Data[2] * mag;
  }

  double& Vector::X()
  {
    return m_Data[0];
  }
  
  double& Vector::Y()
  {
    return m_Data[1];
  }
  
  double& Vector::Z()
  {
    return m_Data[2];
  }
  
  const double& Vector::X() const
  {
    return m_Data[0];
  }
  
  const double& Vector::Y() const
  {
    return m_Data[1];
  }
  
  const double& Vector::Z() const
  {
    return m_Data[2];
  }

  double& Vector::operator[](unsigned char ndx)
  {
    return m_Data[ndx];
  }

  const double& Vector::operator[](unsigned char ndx) const
  {
    return m_Data[ndx];
  }


  Vector Vector::operator+(const Vector& rhs) const
  {
    return( Vector(m_Data[0] + rhs.m_Data[0],
                   m_Data[1] + rhs.m_Data[1],
                   m_Data[2] + rhs.m_Data[2]) );
  }

  Vector Vector::operator+(double rhs) const
  {
    return( Vector(m_Data[0] + rhs, m_Data[1] + rhs, m_Data[2] + rhs) );
  }

  Vector Vector::operator-(const Vector& rhs) const
  {
    return( Vector(m_Data[0] - rhs.m_Data[0],
                   m_Data[1] - rhs.m_Data[1],
                   m_Data[2] - rhs.m_Data[2]) );
  }

  Vector Vector::operator-(double rhs) const
  {
    return( Vector(m_Data[0] - rhs, m_Data[1] - rhs, m_Data[2] - rhs) );
  }

  Vector Vector::operator*(double rhs) const
  {
    return( Vector(m_Data[0] * rhs, m_Data[1] * rhs, m_Data[2] * rhs) );
  }

  Vector Vector::operator/(double rhs) const
  {
    rhs = 1.f / rhs;
    return( Vector(m_Data[0] * rhs, m_Data[1] * rhs, m_Data[2] * rhs) );
  }

  Vector& Vector::operator=(const Vector& rhs)
  {
    if( this != &rhs ){
      m_Data[0] = rhs.m_Data[0];
      m_Data[1] = rhs.m_Data[1];
      m_Data[2] = rhs.m_Data[2];
    }
    
    return *this;
  }

  Vector Vector::operator-() const
  {
    return( Vector(-m_Data[0], -m_Data[1], -m_Data[2]) );
  }

  const Vector& Vector::operator+=(const Vector &rhs)
  {
    m_Data[0] += rhs.m_Data[0];
    m_Data[1] += rhs.m_Data[1];
    m_Data[2] += rhs.m_Data[2];
    return *this; 
  }

  const Vector& Vector::operator-=(const Vector &rhs)
  {
    m_Data[0] -= rhs.m_Data[0];
    m_Data[1] -= rhs.m_Data[1];
    m_Data[2] -= rhs.m_Data[2];
    return *this;
  }

  const Vector& Vector::operator*=(const double rhs)
  {
    m_Data[0] *= rhs;
    m_Data[1] *= rhs;
    m_Data[2] *= rhs;
    return *this;
  }

  const Vector& Vector::operator/=(double rhs)
  {
    rhs = 1.f / rhs;
    m_Data[0] *= rhs;
    m_Data[1] *= rhs;
    m_Data[2] *= rhs;
    return *this;
  }

  // boolean
  bool Vector::operator==(double rhs) const
  {
    return( m_Data[0] == rhs && m_Data[1] == rhs && m_Data[2] == rhs );
  }

  bool Vector::operator!=(double rhs) const
  {
    return( m_Data[0] != rhs || m_Data[1] != rhs || m_Data[2] != rhs );
  }

  bool Vector::operator==(const Vector &rhs) const
  {
    return( m_Data[0] == rhs.m_Data[0] &&
            m_Data[1] == rhs.m_Data[1] &&
            m_Data[2] == rhs.m_Data[2] );
  }

  bool Vector::operator!=(const Vector &rhs) const
  {
    return( m_Data[0] != rhs.m_Data[0] ||
            m_Data[1] != rhs.m_Data[1] ||
            m_Data[2] != rhs.m_Data[2] );
  }
  
  // external functions
  Vector cross(const Vector& lhs, const Vector& rhs)
  {
    return( Vector(lhs.Y() * rhs.Z() - lhs.Z() * rhs.Y(),
                   lhs.Z() * rhs.X() - lhs.X() * rhs.Z(),
                   lhs.X() * rhs.Y() - lhs.Y() * rhs.X()) );
  }
  
  double dot(const Vector& lhs, const Vector& rhs)
  {
    return( lhs.X() * rhs.X() + lhs.Y() * rhs.Y() + lhs.Z() * rhs.Z() );
  }

  Vector operator*(double lhs, const Vector& rhs)
  {
    return( Vector(rhs.X() * lhs, rhs.Y() * lhs, rhs.Z() * lhs) );
  }

  std::ostream& operator<<(std::ostream& out, const Vector& obj)
  {
    return( out << "[x, y, z]: [" << obj.X() << ", " << obj.Y() 
                << ", " << obj.Z() << "]" );
  }
}  // velcro
