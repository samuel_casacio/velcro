#include "MemInCore.h"

/**
 *  \file src/MemInCore.src
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date April 8, 2014
 *  \brief In core memory backing for a mesh.
 */

namespace velcro
{
  namespace mem
  {
    InCore::InCore()
      : m_VertexIdx(0), m_EdgeIdx(0), m_FaceIdx(0)
    {
      return;
    }

    VertexIndex
    InCore::storeVertex(const Vertex& vertex)
    {
      VertexIndex vi(m_VertexIdx++);
      m_Vertices.emplace(vi, vertex);
      return vi;
    }

    EdgeIndex
    InCore::storeEdge(const Edge& edge)
    {
      EdgeIndex ei(m_EdgeIdx++);
      m_Edges.emplace(ei, edge);
      return ei;
    }

    FaceIndex
    InCore::storeFace(const Face& face)
    {
      FaceIndex fi(m_FaceIdx++);
      m_Faces.emplace(fi, face);
      return fi;
    }

    Vertex&
    InCore::getVertex(const VertexIndex index)
    {
      return m_Vertices.find(index)->second;
    }

    const Vertex&
    InCore::getVertex(const VertexIndex index) const
    {
      return m_Vertices.find(index)->second;
    }

    Edge&
    InCore::getEdge(const EdgeIndex index)
    {
      return m_Edges.find(index)->second;
    }

    const Edge&
    InCore::getEdge(const EdgeIndex index) const
    {
      return m_Edges.find(index)->second;
    }

    Face&
    InCore::getFace(const FaceIndex index)
    {
      return m_Faces.find(index)->second;
    }

    const Face&
    InCore::getFace(const FaceIndex index) const
    {
      return m_Faces.find(index)->second;
    }

    void
    InCore::eraseVertex(const VertexIndex index)
    {
      m_Vertices.erase(index);
    }

    void
    InCore::eraseEdge(const EdgeIndex index)
    {
      m_Edges.erase(index);
    }

    void
    InCore::eraseFace(const FaceIndex index)
    {
      m_Faces.erase(index);
    }

    VertexIndex
    InCore::firstVertex() const
    {
      auto it = m_Vertices.lower_bound(VertexIndex(0));
      return it == m_Vertices.end() ? VertexIndex::Invalid() : it->first;
    }

    EdgeIndex
    InCore::firstEdge() const
    {
      auto it = m_Edges.lower_bound(EdgeIndex(0));
      return it == m_Edges.end() ? EdgeIndex::Invalid() : it->first;
    }

    FaceIndex
    InCore::firstFace() const
    {
      auto it = m_Faces.lower_bound(FaceIndex(0));
      return it == m_Faces.end() ? FaceIndex::Invalid() : it->first;
    }

    VertexIndex
    InCore::nextVertex(VertexIndex from) const
    {
      auto it = m_Vertices.upper_bound(from);
      return it == m_Vertices.end() ? VertexIndex::Invalid() : it->first;
    }

    EdgeIndex
    InCore::nextEdge(EdgeIndex from) const
    {
      auto it = m_Edges.upper_bound(from);
      return it == m_Edges.end() ? EdgeIndex::Invalid() : it->first;
    }

    FaceIndex
    InCore::nextFace(FaceIndex from) const
    {
      auto it = m_Faces.upper_bound(from);
      return it == m_Faces.end() ? FaceIndex::Invalid() : it->first;
    }
  }; // mem
}; // velcro
