/**
 *  \file src/Edge.cpp
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date April 8, 2014
 *  \brief Simple structure to store mesh connectivity
 */

#include "Edge.h"

namespace velcro
{
  std::ostream& operator<<(std::ostream& out, const Edge& edge)
  {
    out << "Edge:"
        << "\n\tvertex 0: " << edge.vertex[0]
        << "\n\tvertex 1: " << edge.vertex[1]
        << "\n\tclockwise next in face: " << edge.nextEdgeInFace[0]
        << "\n\tcounter-clockwise next in face: " << edge.nextEdgeInFace[1]
        << "\n\tclockwise next at vertex: " << edge.nextEdgeAtVertex[0]
        << "\n\tcounter-clockwise next at vertex: " << edge.nextEdgeAtVertex[1]
        << "\n\tclockwise face: " << edge.face[0]
        << "\n\tcounter-clockwise face: " << edge.face[1];
    return out;
  }
}; // velcro
