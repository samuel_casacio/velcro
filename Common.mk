#Makefile configuration
BIN_PATH := bin/
OBJ_PATH := .obj
DOC_PATH := doc/
LD := g++
RM := rm -fr
OS := $(shell uname -s)

#Common build options
CXX_FLAGS := -c -o0 -Wall -Werror -g -I./h -std=c++0x
LD_FLAGS :=
