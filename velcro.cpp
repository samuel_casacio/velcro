#include <algorithm>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <map>
#include <set>
#include <vector>

#include "ClosestPointQuery.h"
#include "Mesh.h"
#include "Vector.h"

using namespace velcro;

void
makeMesh(Mesh* mesh, int rows, int columns)
{
  std::map<std::pair<int, int>, VertexIndex> verts;

  for(int cy = 0; cy < (rows) / 8; cy++)
  for(int cx = 0; cx < (columns) / 8; cx++)

  for(int y = cy*8; y < std::min(cy*8+8, columns); ++y){
    for(int x = cx*8; x < std::min(cx*8+8, rows); ++x){
      Vertex v;
      v.position = Vector(double(x), double(y), 0.0);
      VertexIndex vi = mesh->addVertex(v);
      verts.emplace(std::make_pair(x, y), vi);
    }
  }

  for(int cy = 0; cy < (rows - 1) / 8; cy++)
  for(int cx = 0; cx < (columns - 1) / 8; cx++)

  for(int y = cy*8; y < std::min(columns - 1, cy*8+8); ++y){
    for(int x = cx*8; x < std::min(rows - 1, cx*8+8); ++x){
      VertexIndex v0(verts[std::make_pair(x, y)]);
      VertexIndex v1(verts[std::make_pair(x + 1, y)]);
      VertexIndex v2(verts[std::make_pair(x, y + 1)]);
      VertexIndex v3(verts[std::make_pair(x + 1, y + 1)]);
      mesh->createFace({v0, v1, v2});
      mesh->createFace({v1, v3, v2});
    }
  }
}

int main(int argc, char** argv){

  Mesh mesh;
  makeMesh(&mesh, 200, 200);

  std::cout << "Created " << mesh.numberFaces() << " faces\n";

  ClosestPointQuery cpq(mesh);

  Vector v(mesh.vertexCBegin()->position);
  v.X() += 10.0;
  v.Y() += 0.5;
  v.Z() += 2.0;

  std::cout << "The closest point to: " << v << " in 11 is: " << cpq(v, 11.0) << std::endl;

  v.X() -= 30.0;
  v.Y() += 6.0;

  std::cout << "The closest point to: " << v << " in 30 is: " << cpq(v, 30.0) << std::endl;
  std::cout << "The closest point to: " << v << " in 10 is: " << cpq(v, 10.0) << std::endl;

  return 0;
}
