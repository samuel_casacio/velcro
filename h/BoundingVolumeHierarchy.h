#pragma once
#ifndef __BOUNDING_VOLUME_HIERARCHY_H__
#define __BOUNDING_VOLUME_HIERARCHY_H__

/**
 *  \file h/BoundingVolumeHierarchy.h
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date February 2, 2016
 */

#include <memory>
#include <vector>

#include "AABB.h"
#include "Index.h"
#include "Mesh.h"
#include "Vector.h"

namespace velcro
{
  /**
   * \class BVH
   * \brief A simple Bounding Volume Hierarchy
   *
   * Based losely off the BVH outlined in Pharr and Humphreys's PBRT.
   * Skips the linearization step.
   */
  class BVH
  {
    public:
      BVH(const Mesh& m);

      /**
       * Search the BVH for all faces within "dist" distance of "v" point
       * \param[in] v A point in 3d space
       * \param[in] dist Max distance to gather faces, if 0.0 (default) only
       *                 gather faces if point is inside a volume
       * \return A vector of FaceIndex for the backing mesh
       */
      std::vector<FaceIndex> search(const Vector& v, double dist = 0.0) const;

      /**
       * Get a reference to the backing mesh
       * \return A const reference to the backing mesh
       */
      inline const Mesh& getMesh() const
      { return mesh; }

    private:

      // primitive info struct
      struct BVHPrimitiveInfo
      {
        BVHPrimitiveInfo()
        {
          // empty
        }

        BVHPrimitiveInfo(uint64_t pn, const AABB& b):
          primitiveNumber(pn), box(b)
        {
          // empty
        }

        uint64_t primitiveNumber;
        AABB box;
      };

      // build node struct
      struct BVHBuildNode
      {
        BVHBuildNode()
        {
          children[0] = children[1] = NULL;
        }

        void
        initLeaf(uint32_t first, uint32_t n, const AABB& b)
        {
          firstPrimOffset = first;
          nPrimitives = n;
          box = b;
        }

        void
        initInterior(uint32_t axis, std::shared_ptr<BVHBuildNode> c0, std::shared_ptr<BVHBuildNode> c1)
        {
          children[0] = c0;
          children[1] = c1;
          box.merge(c0->box);
          box.merge(c1->box);
          splitAxis = axis;
          nPrimitives = 0;
        }

        AABB box;
        std::shared_ptr<BVHBuildNode> children[2];
        uint32_t splitAxis, firstPrimOffset, nPrimitives;
      };

      // functor for partitioning
      struct ComparePoints
      {
        ComparePoints(unsigned char d):
          dim(d)
        {
          // empty
        }

        bool operator()(const BVHPrimitiveInfo& a,
                        const BVHPrimitiveInfo& b) const
        {
          return a.box.centroid()[dim] < b.box.centroid()[dim];
        }

        unsigned char dim;
      };

      std::shared_ptr<BVHBuildNode> recursiveBuild(std::vector<BVHPrimitiveInfo> &buildData,
                                                   uint32_t start, uint32_t end, uint32_t *totalNodes,
                                                   std::vector<FaceIndex>& orderedPrims);

      void recursiveSearch(const Vector& v, double dist,
                           std::vector<FaceIndex>& faces, const std::shared_ptr<BVHBuildNode>& node) const;

      const Mesh& mesh;
      std::vector<FaceIndex> primitives;
      std::shared_ptr<BVHBuildNode> root;
  };
}; // velcro
#endif
