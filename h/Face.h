#pragma once
#ifndef __FACE_H__
#define __FACE_H__

/**
 *  \file h/Face.h
 *  \author Samuel Casacio
 *  \version 1.2
 *  \date January 29th, 2016
 */

#include "AABB.h"
#include "Index.h"
#include "Vector.h"

namespace velcro
{
  //! \class Face
  //! \brief Simple structure to store attributes of a mesh face
  class Face
  {
    public:
      EdgeIndex edge;
  };
  
  std::ostream& operator<<(std::ostream& out, const Face& face);
}; // velcro
#endif
