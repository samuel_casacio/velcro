#pragma once
#ifndef __INDEX_H__
#define __INDEX_H__

/**
 *  \file h/Index.h
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date April 8, 2014
 */

#include <cstdint>
#include <ostream>

namespace velcro
{

  //! \class Index
  //! \brief Contains declarations of the Index type
  class Index
  {
    protected:
      union {
        struct {
          uint32_t m_Group;
          uint32_t m_Offset;
        };
        uint64_t m_Value;
      };

    public:
      Index()
        : m_Value((uint64_t)-1)
      {}

      Index(uint32_t g, uint32_t o)
        : m_Group(g), m_Offset(o)
      {}

      Index(uint64_t v)
        : m_Value(v)
      {}

      inline bool valid() const
      { return m_Value != (uint64_t)-1;  }
      
      inline uint32_t group() const{ return m_Group; }
      inline uint32_t offset() const{ return m_Offset; }
      inline uint64_t raw() const{ return m_Value; }
  };

  std::ostream& operator<<(std::ostream& out, const Index& index);

  //! \class VertexIndex
  class VertexIndex : public Index
  {
    public:
      inline VertexIndex() : Index() {}
      inline VertexIndex(uint32_t g, uint32_t o) : Index(g, o) {}
      inline VertexIndex(uint64_t v) : Index(v) {}

      inline bool operator<(const VertexIndex &other) const
      { return m_Value < other.m_Value; }

      inline bool operator==(const VertexIndex idx) const
      { return (m_Value == idx.m_Value); }
      
      inline bool operator!=(const VertexIndex idx) const
      { return (m_Value != idx.m_Value); }

      static inline VertexIndex Invalid()
      { return VertexIndex((uint64_t)(-1)); }
  };
  
  //! \class EdgeIndex
  class EdgeIndex : public Index
  {
    public:
      inline EdgeIndex() : Index() {}
      inline EdgeIndex(uint32_t g, uint32_t o) : Index(g, o) {}  
      inline EdgeIndex(uint64_t v) : Index(v) {}

      inline bool operator<(const EdgeIndex &other) const
      { return m_Value < other.m_Value; }

      inline bool operator==(const EdgeIndex idx) const
      { return (m_Value == idx.m_Value); }
      
      inline bool operator!=(const EdgeIndex idx) const
      { return (m_Value != idx.m_Value); }
      
      static inline EdgeIndex Invalid()
      { return EdgeIndex((uint64_t)(-1));  }
  };
  
  //! \class FaceIndex
  class FaceIndex : public Index
  {
    public:
      inline FaceIndex() : Index() {}
      inline FaceIndex(uint32_t g, uint32_t o) : Index(g, o) {}  
      inline FaceIndex(uint64_t v) : Index(v) {}  

      inline bool operator<(const FaceIndex &other) const
      { return m_Value < other.m_Value; }

      inline bool operator==(const FaceIndex idx) const
      { return (m_Value == idx.m_Value); }
      
      inline bool operator!=(const FaceIndex idx) const
      { return (m_Value != idx.m_Value); }
      
      static inline FaceIndex Invalid()
      { return FaceIndex((uint64_t)(-1));  }
  };
}; // velcro
#endif
