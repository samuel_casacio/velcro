#pragma once
#ifndef __WAVEFRONT_H__
#define __WAVEFRONT_H__

/**
 *  \file h/WaveFront.h
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date April 27, 2014
 *  \brief Simple obj loader and saver
 */

#include "Mesh.h"

namespace velcro
{
  //! assumes a blank mesh
  //! \return true if loaded, false otherwise
  bool LoadObj(const char* filename, Mesh* mesh);

  //! \return true if loaded, false otherwise
  bool SaveObj(const char* filename, const Mesh& mesh);
}; // velcro
#endif
