#pragma once
#ifndef __VECTOR_H__
#define __VECTOR_H__

/**
 *  \file h/Vector.h
 *  \author Samuel Casacio
 *  \version 1.4
 *  \date October 9th, 2013
 */

#include <ostream>

namespace velcro
{

  //! \class Vector
  //! \brief 3 dimensional vector class
  class Vector
  {
    private:
      double m_Data[3];
    
    public:
      Vector(double nx = 0.0, double ny = 0.0, double nz = 0.0);
      Vector(const Vector& rhs);

      // important vector stuffs
      double magnitude() const;
      Vector unitvector() const;
      void normalize();

      // data access
      double& X();
      double& Y();
      double& Z();
      const double& X() const;
      const double& Y() const;
      const double& Z() const;

      // overloaded operators
      double& operator[](unsigned char ndx);
      const double& operator[](unsigned char ndx) const;

      // vector maths
      Vector operator+(const Vector& rhs) const;
      Vector operator+(double rhs) const;
      Vector operator-(const Vector& rhs) const;
      Vector operator-(double rhs) const;
      Vector operator*(double rhs) const;
      Vector operator/(double rhs) const;
    
      // assignment
      Vector& operator=(const Vector& rhs);
    
      // negation
      Vector operator-() const;

      // compound operators
      const Vector& operator+=(const Vector& rhs);
      const Vector& operator-=(const Vector& rhs);
      const Vector& operator*=(double rhs);
      const Vector& operator/=(double rhs);

      // boolean operators
      bool operator==(double rhs) const;
      bool operator!=(double rhs) const;
      bool operator==(const Vector& rhs) const;
      bool operator!=(const Vector& rhs) const;
  };

  // external functions
  Vector cross(const Vector& lhs, const Vector& rhs);
  double dot(const Vector& lhs, const Vector& rhs);
  Vector operator*(double lhs, const Vector& rhs);
  std::ostream& operator<<(std::ostream& out, const Vector& obj);

}  // velcro
#endif
