#pragma once
#ifndef __MESH_H__
#define __MESH_H__

/**
 *  \file h/Mesh.h
 *  \author Samuel Casacio
 *  \version 1.2
 *  \date January 29th, 2016
 */

#include <iterator>
#include <iostream>
#include <vector>

#include "Face.h"
#include "Edge.h"
#include "Index.h"
#include "MemInCore.h"
#include "Vector.h"
#include "Vertex.h"

namespace velcro
{
  /**
   *  \class Mesh
   *  \brief Simple half-edge mesh structure
   *  
   *  Note: The backing memory store has been abstracted to a Mem::InCore object.
   *        This is to facilitate future experiments in using cached or paged memory
   *        as the backing store.
   *
   *  TODO: Remove triangle only mesh restriction by implementing GJK.
   */
  class Mesh
  {
    public:
      Mesh();

      /**
       * \brief Add a new vertex to the backing store
       * \param[in] vertex A vertex object in the mesh
       * \return The VertexIndex of the new vertex
       */
      VertexIndex addVertex(const Vertex& vertex);

      /**
       * \brief Add a new face to the backing store
       * \param[in] verts A list of VertexIndex already stored by the mesh
       * \return The FaceIndex of the new face
       *
       * NOTE: Faces must be a triange (artificial restriction)
       */
      FaceIndex createFace(const std::vector<VertexIndex>& verts);

      /**
       * \brief Find the edge containing two verticies
       * \param[in] v0 A VertexIndex in the edge
       * \param[in] v1 A VertexIndex in the edge
       * \return The EdgeIndex if it exists, otherwise EdgeIndex::invalid()
       */
      EdgeIndex lookupEdge(VertexIndex v0, VertexIndex v1) const;

      /**
       * \brief Get the surface normal for a face
       * \param[in] face The FaceIndex of the face to use
       * \return A unit vector for the surface normal
       *
       * TODO: will return vector of -nan if adjacent edges are equal length and
       * opposite vectors
       *
       * TODO: fails silently on >3 sides (quads or higher)
       */
      Vector normal(FaceIndex face) const;

      /**
       * \brief Get the bounding volume for a face
       * \param[in] face The FaceIndex of the face to use
       * \return The AABB object for the desired face
       */
      AABB getFaceBoundingVolume(FaceIndex face) const;

      /**
       * \brief Find the closest point in a face to the specified point in space
       * \param[in] point A point in 3d space
       * \param[in] face A FaceIndex object
       * \return A point on the surface of the mesh
       *
       * Assumes the face is a triangle (need to reimplement using GJK with depth test for
       * generic simplex).
       */
      Vector closestPointInFace(const Vector& point, FaceIndex face) const;
      
      // use the cached counters instead of depending on backing store.
      inline uint64_t numberVerts() const
      { return m_NVerts; }
      inline uint64_t numberEdges() const
      { return m_NEdges; }
      inline uint64_t numberFaces() const
      { return m_NFaces; }

      //! \class VertexIterator
      //! \brief Iterate over all vertices in a mesh
      class VertexIterator :
      public std::iterator<std::forward_iterator_tag, Vertex>
      {
        public:
          inline Vertex& operator*()
          { return m_Mesh.getVertex(m_Current); }

          inline Vertex* operator->()
          { return &m_Mesh.getVertex(m_Current); }
        
          inline VertexIterator& operator++()
          { m_Current = m_Mesh.m_Storage.nextVertex(m_Current); return *this; }
          
          inline VertexIterator operator++(int)
          {
            VertexIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const VertexIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current == rhs.m_Current; }

          inline bool operator!=(const VertexIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current != rhs.m_Current; }
          
          inline VertexIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          VertexIterator(Mesh& m):
          m_Mesh(m), m_Current(m.m_Storage.firstVertex()) {}

          VertexIterator(Mesh& m, VertexIndex i):
          m_Mesh(m), m_Current(i) {}

          Mesh& m_Mesh;
          VertexIndex m_Current;
      };

      //! \class const_VertexIterator
      //! \brief Iterate over all vertices in a mesh
      class const_VertexIterator : 
      public std::iterator<std::forward_iterator_tag, const Vertex>
      {
        public:
          inline const Vertex& operator*() const
          { return m_Mesh.getVertex(m_Current); }
        
          inline const Vertex* operator->() const
          { return &m_Mesh.getVertex(m_Current); }
            
          inline const_VertexIterator& operator++()
          { m_Current = m_Mesh.m_Storage.nextVertex(m_Current); return *this; }
          
          inline const_VertexIterator operator++(int)
          {
            const_VertexIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const const_VertexIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current == rhs.m_Current; }

          inline bool operator!=(const const_VertexIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current != rhs.m_Current; }
          
          inline VertexIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          const_VertexIterator(const Mesh& m):
          m_Mesh(m), m_Current(m.m_Storage.firstVertex()) {}

          const_VertexIterator(const Mesh& m, VertexIndex i):
          m_Mesh(m), m_Current(i) {}

          const Mesh& m_Mesh;
          VertexIndex m_Current;
      };

      //! \class EdgeIterator
      //! \brief Iterate over all edges in a mesh
      class EdgeIterator :
      public std::iterator<std::forward_iterator_tag, const Edge>
      {
        public:
          inline const Edge& operator*() const
          { return m_Mesh.getEdge(m_Current); }
        
          inline const Edge* operator->() const
          { return &m_Mesh.getEdge(m_Current); }
            
          inline EdgeIterator& operator++()
          { m_Current = m_Mesh.m_Storage.nextEdge(m_Current); return *this; }
          
          inline EdgeIterator operator++(int)
          {
            EdgeIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const EdgeIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current == rhs.m_Current; }

          inline bool operator!=(const EdgeIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current != rhs.m_Current; }
          
          inline EdgeIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          EdgeIterator(const Mesh& m):
          m_Mesh(m), m_Current(m.m_Storage.firstEdge()) {}

          EdgeIterator(const Mesh& m, EdgeIndex i):
          m_Mesh(m), m_Current(i) {}

          const Mesh& m_Mesh;
          EdgeIndex m_Current;
      };

      //! \class FaceIterator
      //! \brief Iterate over all faces in a mesh
      class FaceIterator :
      public std::iterator<std::forward_iterator_tag, const Face>
      {
        public:
          inline const Face& operator*() const
          { return m_Mesh.getFace(m_Current); }
        
          inline const Face* operator->() const
          { return &m_Mesh.getFace(m_Current); }
            
          inline FaceIterator& operator++()
          { m_Current = m_Mesh.m_Storage.nextFace(m_Current); return *this; }
          
          inline FaceIterator operator++(int)
          {
            FaceIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const FaceIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current == rhs.m_Current; }

          inline bool operator!=(const FaceIterator& rhs) const
          { return &m_Mesh == &rhs.m_Mesh && m_Current != rhs.m_Current; }
          
          inline FaceIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          FaceIterator(const Mesh& m):
          m_Mesh(m), m_Current(m.m_Storage.firstFace()) {}

          FaceIterator(const Mesh& m, FaceIndex i):
          m_Mesh(m), m_Current(i) {}

          const Mesh& m_Mesh;
          FaceIndex m_Current;
      };

      //! \class VertexEdgeIterator
      //! \brief Iterate over all edges connected to a vertex
      class VertexEdgeIterator : 
      public std::iterator<std::forward_iterator_tag, const Edge>
      {
        public:
          inline const Edge& operator*() const
          { return m_Mesh.getEdge(m_Current); }
        
          inline const Edge* operator->() const
          { return &m_Mesh.getEdge(m_Current); }
            
          inline VertexEdgeIterator& operator++()
          {
            m_Current = m_Mesh.m_Storage.getEdge(m_Current).next(m_Vertex);
            return *this;
          }
          
          inline VertexEdgeIterator operator++(int)
          {
            VertexEdgeIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const VertexEdgeIterator& rhs) const
          {
            return  &m_Mesh == &rhs.m_Mesh && 
                    m_Current == rhs.m_Current &&
                    m_Vertex == rhs.m_Vertex;
          }

          inline bool operator!=(const VertexEdgeIterator& rhs) const
          {
            return  &m_Mesh == &rhs.m_Mesh && 
                    m_Current != rhs.m_Current &&
                    m_Vertex == rhs.m_Vertex;
          }
          
          inline EdgeIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          VertexEdgeIterator(const Mesh& m, VertexIndex idx):
          m_Mesh(m), m_Vertex(idx), m_Current(m.m_Storage.getVertex(idx).edge) {}

          VertexEdgeIterator(const Mesh& m, VertexIndex idx, EdgeIndex eidx):
          m_Mesh(m), m_Vertex(idx), m_Current(eidx) {}
        
          const Mesh& m_Mesh;
          VertexIndex m_Vertex;
          EdgeIndex m_Current;
      };

      //! \class FaceVertexIterator
      //! \brief Iterate over all the vertices in a face
      class FaceVertexIterator :
      public std::iterator<std::forward_iterator_tag, Vertex>
      {
        public:
          inline Vertex& operator*()
          { return m_Mesh.getVertex(m_Current); }
        
          inline Vertex* operator->()
          { return &m_Mesh.getVertex(m_Current); }
        
          inline FaceVertexIterator& operator++()
          {
            m_Edge = m_Mesh.m_Storage.getEdge(m_Edge).next(m_Face);
            
            if(m_Edge == m_Mesh.m_Storage.getFace(m_Face).edge){
              m_Edge = EdgeIndex::Invalid();
              m_Current = VertexIndex::Invalid();      
            }
            else{
              m_Current = m_Mesh.m_Storage.getEdge(m_Edge).opposite(m_Current);
            }
            
            return *this;
          }
          
          inline FaceVertexIterator operator++(int)
          {
            FaceVertexIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const FaceVertexIterator& rhs) const
          { 
            return  &m_Mesh == &rhs.m_Mesh &&
                    m_Face == rhs.m_Face &&
                    m_Edge == rhs.m_Edge &&
                    m_Current == rhs.m_Current;
          }

          inline bool operator!=(const FaceVertexIterator& rhs) const
          { 
            return  &m_Mesh == &rhs.m_Mesh &&
                    m_Face == rhs.m_Face &&
                    m_Edge != rhs.m_Edge &&
                    m_Current != rhs.m_Current;
          }
          
          inline VertexIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          FaceVertexIterator(Mesh& m, FaceIndex idx):
          m_Mesh(m), m_Face(idx), m_Edge(m.m_Storage.getFace(idx).edge),
          m_Current(m.m_Storage.getEdge(m_Edge).vertex[0])
          {
            if(!(m.m_Storage.getEdge(m.m_Storage.getEdge(m_Edge).next(idx)).contains(m_Current)))
            { 
              m_Current = m.m_Storage.getEdge(m_Edge).vertex[1];
            }
          }

          FaceVertexIterator(Mesh& m, FaceIndex idx, EdgeIndex eidx, VertexIndex vidx):
          m_Mesh(m), m_Face(idx), m_Edge(eidx), m_Current(vidx) {}
        
          Mesh& m_Mesh;
          FaceIndex m_Face;
          EdgeIndex m_Edge;
          VertexIndex m_Current;
      };

      //! \class const_FaceVertexIterator
      //! \brief Iterate over all the vertices in a face
      class const_FaceVertexIterator :
      public std::iterator<std::forward_iterator_tag, const Vertex>
      {
        public:
          inline const Vertex& operator*() const
          { return m_Mesh.getVertex(m_Current); }

          inline const Vertex* operator->() const
          { return &m_Mesh.getVertex(m_Current); }
            
          inline const_FaceVertexIterator& operator++()
          {
            m_Edge = m_Mesh.m_Storage.getEdge(m_Edge).next(m_Face);
            
            if(m_Edge == m_Mesh.m_Storage.getFace(m_Face).edge){
              m_Edge = EdgeIndex::Invalid();
              m_Current = VertexIndex::Invalid();      
            }
            else{
              m_Current = m_Mesh.m_Storage.getEdge(m_Edge).opposite(m_Current);
            }
            
            return *this;
          }
          
          inline const_FaceVertexIterator operator++(int)
          {
            const_FaceVertexIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const const_FaceVertexIterator& rhs) const
          { 
            return  &m_Mesh == &rhs.m_Mesh &&
                    m_Face == rhs.m_Face &&
                    m_Edge == rhs.m_Edge &&
                    m_Current == rhs.m_Current;
          }

          inline bool operator!=(const const_FaceVertexIterator& rhs) const
          { 
            return  &m_Mesh == &rhs.m_Mesh &&
                    m_Face == rhs.m_Face &&
                    m_Edge != rhs.m_Edge &&
                    m_Current != rhs.m_Current;
          }
          
          inline VertexIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          const_FaceVertexIterator(const Mesh& m, FaceIndex idx):
          m_Mesh(m), m_Face(idx), m_Edge(m.m_Storage.getFace(idx).edge),
          m_Current(m.m_Storage.getEdge(m_Edge).vertex[0])
          {
            if(!(m.m_Storage.getEdge(m.m_Storage.getEdge(m_Edge).next(idx)).contains(m_Current)))
            { 
              m_Current = m.m_Storage.getEdge(m_Edge).vertex[1];
            }
          }

          const_FaceVertexIterator(const Mesh& m, FaceIndex idx, EdgeIndex eidx, VertexIndex vidx):
          m_Mesh(m), m_Face(idx), m_Edge(eidx), m_Current(vidx) {}
        
          const Mesh& m_Mesh;
          FaceIndex m_Face;
          EdgeIndex m_Edge;
          VertexIndex m_Current;
      };

      //! \class FaceEdgeIterator
      //! \brief Iterate over all edges in a face
      class FaceEdgeIterator :
      public std::iterator<std::forward_iterator_tag, const Edge>
      {
        public:
          inline const Edge& operator*() const
          { return m_Mesh.getEdge(m_Current); }
        
          inline const Edge* operator->() const
          { return &m_Mesh.getEdge(m_Current); }
            
          inline FaceEdgeIterator& operator++()
          {
            m_Current = m_Mesh.m_Storage.getEdge(m_Current).next(m_Face);
            if(m_Current == m_Mesh.m_Storage.getFace(m_Face).edge) m_Current = EdgeIndex::Invalid();
            return *this;
          }
          
          inline FaceEdgeIterator operator++(int)
          {
            FaceEdgeIterator tmp(*this);
            this->operator++();
            return tmp;
          }

          inline bool operator==(const FaceEdgeIterator& rhs) const
          {
            return  &m_Mesh == &m_Mesh && 
                    m_Current == rhs.m_Current &&
                    m_Face == rhs.m_Face;
          }

          inline bool operator!=(const FaceEdgeIterator& rhs) const
          {
            return  &m_Mesh == &m_Mesh && 
                    m_Current != rhs.m_Current &&
                    m_Face == rhs.m_Face;
          }
          
          inline EdgeIndex idx() const
          { return m_Current; }

        private:
          friend Mesh;
          FaceEdgeIterator(const Mesh& m, FaceIndex idx):
          m_Mesh(m), m_Face(idx), m_Current(m.m_Storage.getFace(idx).edge) {}

          FaceEdgeIterator(const Mesh& m, FaceIndex idx, EdgeIndex eidx):
          m_Mesh(m), m_Face(idx), m_Current(eidx) {}
        
          const Mesh& m_Mesh;
          FaceIndex m_Face;
          EdgeIndex m_Current;
      };



      // vertex lookup functions
      inline Vertex& getVertex(VertexIndex index)
      { return m_Storage.getVertex(index); }
      inline const Vertex& getVertex(VertexIndex index) const
      { return m_Storage.getVertex(index); }
      inline Vertex& operator[](VertexIndex index)
      { return m_Storage.getVertex(index); }
      inline const Vertex& operator[](VertexIndex index) const
      { return m_Storage.getVertex(index); }

      // edge lookup functions
      inline const Edge& getEdge(EdgeIndex index) const
      { return m_Storage.getEdge(index); }
      inline const Edge& operator[](EdgeIndex index) const
      { return m_Storage.getEdge(index); }

      // face lookup functions
      inline const Face& getFace(FaceIndex index) const
      { return m_Storage.getFace(index); }
      inline const Face& operator[](FaceIndex index) const
      { return m_Storage.getFace(index); }

      // vertex iterator lookup functions
      inline VertexIterator vertexBegin()
      { return VertexIterator(*this); }
      inline VertexIterator vertexEnd()
      { return VertexIterator(*this, VertexIndex::Invalid()); }

      inline const_VertexIterator vertexCBegin() const
      { return const_VertexIterator(*this); }
      inline const_VertexIterator vertexCEnd() const
      { return const_VertexIterator(*this, VertexIndex::Invalid()); }

      // vertex iterator for a face lookup functions
      inline FaceVertexIterator faceVertexBegin(FaceIndex idx)
      { return FaceVertexIterator(*this, idx); }
      inline FaceVertexIterator faceVertexEnd(FaceIndex idx)
      { return FaceVertexIterator(*this, idx, EdgeIndex::Invalid(), VertexIndex::Invalid()); }

      inline const_FaceVertexIterator faceVertexCBegin(FaceIndex idx) const
      { return const_FaceVertexIterator(*this, idx); }
      inline const_FaceVertexIterator faceVertexCEnd(FaceIndex idx) const
      {
        return const_FaceVertexIterator(
          *this, idx, EdgeIndex::Invalid(), VertexIndex::Invalid()
        );
      }

      // edge iterator lookup functions
      inline EdgeIterator edgeBegin() const
      { return EdgeIterator(*this); }
      inline EdgeIterator edgeEnd() const
      { return EdgeIterator(*this, EdgeIndex::Invalid()); }

      // edge iterator for a vertex lookup functions
      inline VertexEdgeIterator vertexEdgeBegin(VertexIndex vi) const
      { return VertexEdgeIterator(*this, vi); }
      inline VertexEdgeIterator vertexEdgeEnd(VertexIndex vi) const
      { return VertexEdgeIterator(*this, vi, EdgeIndex::Invalid()); }

      // edge iterator for a face lookup functions
      inline FaceEdgeIterator faceEdgeBegin(FaceIndex fi) const
      { return FaceEdgeIterator(*this, fi); }
      inline FaceEdgeIterator faceEdgeEnd(FaceIndex fi) const
      { return FaceEdgeIterator(*this, fi, EdgeIndex::Invalid()); }

      // face iterator lookup functions
      inline FaceIterator faceBegin() const
      { return FaceIterator(*this); }
      inline FaceIterator faceEnd() const
      { return FaceIterator(*this, FaceIndex::Invalid()); }

      // TODO: clear function

    private:
      mem::InCore m_Storage;
      
      // cache counters independently of backing data store.
      uint64_t m_NVerts;
      uint64_t m_NEdges;
      uint64_t m_NFaces;
  };
};  // velcro
#endif
