#pragma once
#ifndef __EDGE_H__
#define __EDGE_H__

/**
 *  \file h/Edge.h
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date April 8, 2014
 */

#include "Index.h"

namespace velcro
{
  /**
   *  \class Edge
   *  \brief Simple structure to store mesh connectivity
   *  All variables should always be valid, with the exception of
   *  one face possibly being invalid.  This is to designate that
   *  the boundary of the mesh is on either the CW or CCW side of
   *  this edge.  If an edge doesnt have a face, then it shouldn't exist.
   */
  class Edge
  {
    public:
      VertexIndex vertex[2];
      EdgeIndex nextEdgeInFace[2];
      EdgeIndex nextEdgeAtVertex[2];
      FaceIndex face[2];

      /**
       * \brief Get the vertex opposite to the specified one in this edge
       * \param[in] v A vertex index in this edge
       * \return The opposite vertex index if the index is in the edge,
       *         otherwise the primary vertex
       */
      inline VertexIndex opposite(VertexIndex v) const
      { return vertex[ vertex[0] == v ? 1 : 0 ]; }

      /**
       * \brief Get the face opposite to the specified one in this edge
       * \param[in] f A face index with this edge
       * \return The opposite face index if the index is in the edge,
       *         otherwise the primary face
       */
      inline FaceIndex opposite(FaceIndex f) const
      { return face[ face[0] == f ? 1 : 0]; }

      /**
       * \brief Get the next edge at the specified vertex
       * \param[in] v A vertex index in this edge
       * \return The next 
       */
      inline EdgeIndex next(VertexIndex v) const
      { return nextEdgeAtVertex[ vertex[0] == v ? 0 : 1 ]; }

      /**
       * \brief Get the next edge at the specified face
       * \param[in] f A face index with this edge
       * \return The next edge in the specified face,
       *         otherwise the primary edge
       */
      inline EdgeIndex next(FaceIndex f) const
      { return nextEdgeInFace[ face[0] == f ? 0 : 1 ]; }
      
      /**
       * \brief Check if a vertex belongs to this edge
       * \param[in] v A vertex index in this edge
       * \return true if the vertex belongs to this edge,
       *         otherwise false
       */
      inline bool contains(VertexIndex v) const
      { return (v == vertex[0] || v == vertex[1]); }

      /**
       * \brief Check if a face contains this edge
       * \param[in] f A face index with this edge
       * \return true if this edge belongs to the specified face,
       *         otherwise false
       */
      inline bool contains(FaceIndex f) const
      { return (f == face[0] || f == face[1]); }
  };
  
  std::ostream& operator<<(std::ostream& out, const Edge& edge);
}; // velcro
#endif
