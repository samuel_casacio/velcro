#pragma once
#ifndef __OBJECTFILEFORMAT_H__
#define __OBJECTFILEFORMAT_H__

/**
 *  \file h/ObjectFileFormat.h
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date May 14th, 2014
 *  \brief Simple off loader and saver
 */

#include "Mesh.h"

namespace velcro
{
  //! assumes a blank mesh
  //! \return true if loaded, false otherwise
  bool LoadOff(const char* filename, Mesh* mesh);

  //! \return true if loaded, false otherwise
  bool SaveOff(const char* filename, const Mesh& mesh);
}; // velcro
#endif
