#pragma once
#ifndef __MEM_IN_CORE_H__
#define __MEM_IN_CORE_H__

/**
 *  \file h/MemInCore.h
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date April 8, 2014
 */

#include <cassert>
#include <map>

#include "Face.h"
#include "Edge.h"
#include "Index.h"
#include "Vector.h"
#include "Vertex.h"

namespace velcro
{
  namespace mem
  {
    //! \class InCore
     //! \brief In core memory backing for a mesh.
    class InCore {
      public:
        InCore();

        /**
         * \brief Add a new vertex to the mesh
         * \param[in] vertex A Vertex object
         * \return A VertexIndex to the vertex in the mesh
         */
        VertexIndex storeVertex(const Vertex &vertex);

        /**
         * \brief Add a new edge to the mesh
         * \param[in] edge An Edge object
         * \return An EdgeIndex to the edge in the mesh
         */
        EdgeIndex storeEdge(const Edge& edge);

        /**
         * \brief Add a new face to the mesh
         * \param[in] face A Face object
         * \return A FaceIndex to the face in the mesh
         */
        FaceIndex storeFace(const Face& face);

        /**
         * \brief Get a vertex from the mesh
         * \param[in] index The VertexIndex of the vertex
         * \return A reference to the vertex in the mesh
         */
        Vertex& getVertex(const VertexIndex index);

        /**
         * \brief Get a vertex from the mesh
         * \param[in] index The VertexIndex of the vertex
         * \return A reference to the vertex in the mesh
         */
        const Vertex &getVertex(const VertexIndex index) const;

        /**
         * \brief Get an edge from the mesh
         * \param[in] index The EdgeIndex of the edge
         * \return A reference to the edge in the mesh
         */
        Edge& getEdge(const EdgeIndex index);

        /**
         * \brief Get an edge from the mesh
         * \param[in] index The EdgeIndex of the edge
         * \return A reference to the edge in the mesh
         */
        const Edge &getEdge(const EdgeIndex index) const;

        /**
         * \brief Get a face from the mesh
         * \param[in] index The FaceIndex of the face
         * \return A reference to the Face in the mesh
         */
        Face& getFace(const FaceIndex index);

        /**
         * \brief Get a face from the mesh
         * \param[in] index The FaceIndex of the face
         * \return A reference to the Face in the mesh
         */
        const Face &getFace(const FaceIndex index) const;

        //! \brief Erase a vertex from the mesh
        //! \param[in] index The VertexIndex of the vertex in the mesh
        void eraseVertex(const VertexIndex index);

        //! \brief Erase an edge from the mesh
        //! \param[in] index The EdgeIndex of the edge in the mesh
        void eraseEdge(const EdgeIndex index);

        //! \brief Erase a face from the mesh
        //! \param[in] index The FaceIndex of the face in the mesh
        void eraseFace(const FaceIndex index);

        //! \brief Get the first vertex in the mesh
        //! \return The VertexIndex of the first vertex in the mesh
        VertexIndex firstVertex() const;

        //! \brief Get the first edge in the mesh
        //! \return The EdgeIndex of the first edge in the mesh
        EdgeIndex firstEdge() const;

        //! \brief Get the first face in the mesh
        //! \return The FaceIndex of the first face in the mesh
        FaceIndex firstFace() const;

        /**
         * \brief Get the next VertexIndex in the mesh
         * \param[in] from The current VertexIndex
         * \return The VertexIndex of the next vertex in the mesh
         *
         * This function is used to iterate sequentually over vertices
         * currently in memory. Useful if not all objects are in memory.
         */
        VertexIndex nextVertex(VertexIndex from) const;

        /**
         * \brief Get the next EdgeIndex in the mesh
         * \param[in] from The current EdgeIndex
         * \return The EdgeIndex of the next edge in the mesh
         *
         * This function is used to iterate sequentually over edges
         * currently in memory. Useful if not all objects are in memory.
         */
        EdgeIndex nextEdge(EdgeIndex from) const;

        /**
         * \brief Get the next FaceIndex in the mesh
         * \param[in] from The current FaceIndex
         * \return The FaceIndex of the next face in the mesh
         *
         * This function is used to iterate sequentually over faces
         * currently in memory. Useful if not all objects are in memory.
         */
        FaceIndex nextFace(FaceIndex from) const;
      
      private:
        std::map<VertexIndex, Vertex> m_Vertices;
        std::map<EdgeIndex, Edge> m_Edges;
        std::map<FaceIndex, Face> m_Faces;
        uint64_t m_VertexIdx, m_EdgeIdx, m_FaceIdx;
    };
  }; // mem
}; // velcro
#endif
