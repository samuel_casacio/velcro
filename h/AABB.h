#pragma once
#ifndef __AABB_H__
#define __AABB_H__

/**
 *  \file h/AABB.h
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date January 31, 2016
*/

#include "Vector.h"

namespace velcro
{
  /*
   * \class AABB
   * \brief Simple axis aligned bounding box
   */
  class AABB
  {
    public:
      AABB(const Vector& min, const Vector& max);
      AABB(const AABB& box);
      AABB();
    
      /**
       * Find the distance to the closest surface of the BB, will return 0 if inside
       * \param[in] point A point in space
       * \return Min distance if point outside the BB, otherwise 0 if inside
       */
      double distance(const Vector& point) const;

      /**
       * Check if point "p" is inside the BB
       * \param[in] p A point in space
       * \return true if inside BB, else false
       */
      inline bool inside(const Vector& p) const
      {
        return (p.X() <= bounds[1].X() && p.X() >= bounds[0].X() &&
                p.Y() <= bounds[1].Y() && p.Y() >= bounds[0].Y() &&
                p.Z() <= bounds[1].Z() && p.Z() >= bounds[0].Z()   );
      }

      // value accessors
      inline const Vector& min() const
      { return bounds[0]; }
      inline const Vector& max() const
      { return bounds[1]; }
      inline const Vector& centroid() const
      { return center; }

      void reset(const Vector& min, const Vector& max);
      void reset(const AABB& box);

      // merge together with new data
      void merge(const Vector& point);
      void merge(const AABB& box);
   
      int maximumExtent() const;

    private:
      Vector bounds[2];
      Vector center;

      inline void updateCenter()
      { center = 0.5 * bounds[0] + 0.5 * bounds[1]; }

  };
  std::ostream& operator<<(std::ostream& out, const AABB& rhs);
};  // velcro
#endif
