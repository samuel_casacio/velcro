#pragma once
#ifndef __CLOSEST_POINT_QUERY_H__
#define __CLOSEST_POINT_QUERY_H__

/**
 *  \file h/ClosestPointQuery.h
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date February 2, 2016
 */

#include "BoundingVolumeHierarchy.h"
#include "Mesh.h"
#include "Vector.h"

namespace velcro
{
  /**
   * \class ClosestPointQuery
   * \brief Object for querying a mesh for the closest point to an input point
   *
   * Gathers a subset of the number of triangles in a mesh near a point
   * by querying a BVH tree.  Then linearly finds the closest point on
   * the triangles.  If the mesh is well formed (no overlapping triangles)
   * then query time should be fast.
   */
  class ClosestPointQuery
  {
    public:
      ClosestPointQuery(const Mesh& m);

      /**
       * \brief Query the mesh for the closest point within some distance
       * \param[in] p A point in space
       * \param[in] dist The maximum distance to the point
       * \return The point closest to p within the specified dist...
       *         if there isn't a point within dist will return vector of inf.
       */
      Vector operator()(const Vector& p, double dist) const;

    private:
      BVH bvh;
  };
}; // velcro
#endif
