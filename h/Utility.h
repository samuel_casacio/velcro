#pragma once
#ifndef __UTILITY_H__
#define __UTILITY_H__

/**
 *  \file h/Utility.h
 *  \author Samuel Casacio
 *  \version 1.0
 *  \date January 31, 2016
 */

#include "Vector.h"

namespace velcro
{
  namespace utility
  {
    /**
     * Find the closest point on an edge from a point in 3d space
     * \param[in] v0 The first vertex belonging to the edge
     * \param[in] v1 The second vertex belonging to the edge
     * \param[in] p The point in 3d space
     * \return The point on the edge closest to p
     */
    Vector closestPointInEdge(const Vector& v0, const Vector& v1, const Vector& p);

    /**
     * Find the closest point in a plane in 3d space
     * \param[in] v A point on the plane
     * \param[in] n Plane surface normal
     * \param[in] p The point in 3d space
     * \return The point in the place closest to p
     */
    inline Vector closestPointOnPlane(const Vector& v, const Vector& n, const Vector& p)
    { return p - (dot(v - p, n) * n); }

    /**
     * Find the closest point on a triangle from a point in 3d space
     * \param[in] v0 The first vertex belonging to the triangle
     * \param[in] v1 The second vertex belonging to the triangle
     * \param[in] v2 The third vertex belonging to the triangle
     * \param[in] p The point in 3d space
     * \return The point on the triangle closest to p
     */
    Vector closestPointInTriangle(const Vector& v0, const Vector& v1, const Vector& v2, const Vector& p);
  }; // utility
}; // velcro
#endif
