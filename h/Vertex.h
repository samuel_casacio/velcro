#pragma once
#ifndef __VERTEX_H__
#define __VERTEX_H__

/**
 *  \file h/Vertex.h
 *  \author Samuel Casacio
 *  \version 1.1
 *  \date April 8, 2014
 */

#include <ostream>

#include "Index.h"
#include "Vector.h"

namespace velcro
{
  //! \class Vertex
  //! \brief Simple structure to store attributes of a mesh vertex
  class Vertex
  {
    public:
      EdgeIndex edge;
      Vector position;
  };
  
  std::ostream& operator<<(std::ostream& out, const Vertex& vertex);
}; // velcro
#endif
